﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Iteration
{
    public class TimeMeasurment
    {

        IEnumerable<int> _colletion = null;
        Stopwatch _stopwatch = null;
        bool _isFirstIteration = true;


        public TimeMeasurment(int collectionSize)
        {
            _stopwatch = new Stopwatch();
            _colletion = Enumerable.Range(1, collectionSize);
        }


        public long ListIterationWithForLoop()
        {
            var nums = _colletion.ToList();
            _isFirstIteration = true;
            _stopwatch.Reset();
            int num = 0;
            for (int i = 0; i < nums.Count; i++)
            {
                if (_isFirstIteration)
                {
                    _isFirstIteration = false;
                    _stopwatch.Start();
                }
                num = nums[i];
                //some code
            }
            _stopwatch.Stop();
            return _stopwatch.ElapsedTicks;

        }

        public long ArrayIterationWithForeachLoop()
        {
            var nums = _colletion.ToArray();
            _isFirstIteration = true;
            _stopwatch.Reset();
            foreach (var num in nums)
            {
                if (_isFirstIteration)
                {
                    _isFirstIteration = false;
                    _stopwatch.Start();
                }
                //some code
            }
            _stopwatch.Stop();
            return _stopwatch.ElapsedTicks;
        }

        public long ListIterationWithForeachLoop()
        {
            
            var nums = _colletion.ToList();
            _isFirstIteration = true;
            _stopwatch.Reset();
            foreach (var num in nums)
            {
                if (_isFirstIteration)
                {
                    _isFirstIteration = false;
                    _stopwatch.Start();
                }
                //some code
            }
            _stopwatch.Stop();
            return _stopwatch.ElapsedTicks;
        }

        public long ArrayIterationWithForLoop()
        {
            _stopwatch.Reset();
            _isFirstIteration = true;
            int num = 0;
            var nums = _colletion.ToArray();
            for (int i = 0; i < nums.Length; i++)
            {
                if (_isFirstIteration)
                {
                    _isFirstIteration = false;
                    _stopwatch.Start();
                }
                num = nums[i];         
                //some code
            }
            _stopwatch.Stop();
            return _stopwatch.ElapsedTicks;
        }



    }
}
