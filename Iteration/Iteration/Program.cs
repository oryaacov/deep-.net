﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iteration
{
    class Program
    {
        static void Main(string[] args)
        {
            MeasureTicks(200000000);
        }

        private static void MeasureTicks(int collectionSize)
        {
            TimeMeasurment timeMeasurment = new TimeMeasurment(collectionSize);
            Console.WriteLine("Array with for loop:{0} ticks", timeMeasurment.ArrayIterationWithForLoop());
            Console.WriteLine("Array with foreach loop:{0} ticks", timeMeasurment.ArrayIterationWithForeachLoop());
            Console.WriteLine("List with for loop:{0} ticks", timeMeasurment.ListIterationWithForLoop());
            Console.WriteLine("List with foreach loop:{0} ticks", timeMeasurment.ListIterationWithForeachLoop());
        }

        private static void ListIterationWithForLoop()
        {
            var nums = Enumerable.Range(1, 333).ToList();
            int num = 0;
            for (int i = 0; i < nums.Count; i++)
            {
                num = nums[i];
            } 
        }

        private static void ArrayIterationWithForeachLoop()
        {
            var nums = Enumerable.Range(1, 333).ToArray();
            int i = nums.Length;
            foreach (var num in nums)
            {
                //some code
            }
        }

        private static void ListIterationWithForeachLoop()
        {
            var nums = Enumerable.Range(1, 333).ToList();
            foreach (   var num in nums)
            {
                //some code
            }
        }

        private static void ArrayIterationWithForLoop()
        {
            var nums = Enumerable.Range(1, 333).ToArray();
            int num = 0;
            for (int i = 0; i < nums.Length; i++)
            {
                num = nums[i];
                //some code
            }
        }

        private static void WarmUp()
        {
            for (int i = 0; i < 333; i++)
            {
                //some code
            }
        }


    }
}
